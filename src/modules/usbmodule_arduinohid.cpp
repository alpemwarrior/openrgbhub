#ifdef USE_ARDUINO_HID_MODULE

#include "usbmodule_arduinohid.hpp"
#include "openrgbprotocol.hpp"
#include <RawHID.h>

bool errorhappened = false;

void ArduinoHIDUSBModule::begin(hid_read_data_t * read_packet, hid_write_data_t * write_packet)
{
    RawHID.begin(buffer, sizeof(buffer));
}

bool ArduinoHIDUSBModule::readPacket(hid_read_data_t * packet)
{
    bytes_read = RawHID.poll();

    if (bytes_read != OPENRGB_PROTOCOL_STANDARD_SIZE) 
    {
        return false;
    }

    packet->size = RawHID.read();

    if (packet->size != bytes_read) 
    {
        RawHID.flush();
        return false;
    }
    
    packet->command = RawHID.read();
    packet->data_pointer = buffer + 2;

    return true;
}

void ArduinoHIDUSBModule::writePacket(hid_write_data_t * packet)
{
    RawHID.write(packet->raw_data, packet->size);
}

#endif