#ifdef USE_ARDUINO_ANALOG_ZONE

#include "arduino_analog_zone.hpp"
#include <Arduino.h>

ArduinoAnalogZone::ArduinoAnalogZone(uint8_t pin_r, uint8_t pin_g, uint8_t pin_b, unsigned char * name)
{
    this->pin_r = pin_r;
    this->pin_g = pin_g;
    this->pin_b = pin_b;
    info.name = (uint8_t*)name;
    info.size_max = 1;
    info.type = ZONE_TYPE_SINGLE;
    resize(1);
}

void ArduinoAnalogZone::begin()
{
    pinMode(pin_r, OUTPUT);
    pinMode(pin_g, OUTPUT);
    pinMode(pin_b, OUTPUT);
}

void ArduinoAnalogZone::update()
{
    analogWrite(pin_r, red);
    analogWrite(pin_g, green);
    analogWrite(pin_b, blue);
}

void ArduinoAnalogZone::setLED(uint8_t index, uint8_t red, uint8_t green, uint8_t blue)
{
    this->red = red;
    this->green = green;
    this->blue = blue;
}

uint8_t ArduinoAnalogZone::getLEDR(uint8_t index)
{
    return red;
}

uint8_t ArduinoAnalogZone::getLEDG(uint8_t index)
{
    return green;
}

uint8_t ArduinoAnalogZone::getLEDB(uint8_t index)
{
    return blue;
}

#endif