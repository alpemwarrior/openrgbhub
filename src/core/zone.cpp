#include "zone.hpp"
#include "utils.hpp"

void Zone::clear()
{
    for (int i = 0; i < info.size; i++)
    {
        setLED(i, 0, 0, 0);
    }
}

void Zone::resize(uint8_t new_size)
{
    /* Turn off any leds outside of the new size */
    for (int i = new_size; i < info.size; i++)
    {
        setLED(i, 0, 0, 0);
    }
    info.size = new_size;
}

zone_info_t * Zone::getInfo()
{
    return &info;
}

Effect * Zone::getEffect()
{
    return effect;
}

void Zone::setEffect(Effect * new_effect)
{
    if (effect)
    {
        delete effect;
    }
    effect = new_effect;
}

void Zone::updateEffect()
{
    if(effect) effect->update();
}