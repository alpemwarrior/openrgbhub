#include "rgbmanager.hpp"
#include "utils.hpp"
#include "openrgbprotocol.hpp"

RGBManager::RGBManager(Zone ** zone_list, uint8_t zone_num, effect_constructor_t ** effect_constructor_list, uint8_t effect_num)
{
    this->zone_num = zone_num;
    this->zone_list = zone_list;
    this->effect_constructor_list = effect_constructor_list;
    this->effect_num = effect_num;
}

void RGBManager::begin()
{
    for (uint8_t i = 0; i < zone_num; i++)
    {
        zone_list[i]->begin();
    }
}

void RGBManager::parseSetLED(hid_read_data_t * packet)
{
    uint8_t zone_index = packet->read();
    uint8_t led_index = packet->read();
    uint8_t led_num = zone_list[zone_index]->getInfo()->size - led_index;
    
    if (led_num > 20) led_num = 20;  
    for (uint8_t i = 0; i < led_num; i++)
    {
        uint8_t r = packet->read();
        uint8_t g = packet->read();
        uint8_t b = packet->read();
        zone_list[zone_index]->setLED(
            led_index + i,
            r, g, b
        );
    }

    /* Remove effect if present */
    if (zone_list[zone_index]->getEffect()) zone_list[zone_index]->setEffect(nullptr);
    
    return;
}

void RGBManager::parseGetZoneInfo(hid_read_data_t * in_packet, hid_write_data_t * out_packet)
{
    uint8_t index = in_packet->read();
    
    out_packet->size = OPENRGB_PROTOCOL_STANDARD_SIZE;
    out_packet->command = COMMAND_ZONE_REPORT;

    zone_info_t * info = zone_list[index]->getInfo();

    out_packet->data[0] = index;
    out_packet->data[1] = info->features;
    out_packet->data[2] = info->size;
    out_packet->data[3] = info->size_max;
    out_packet->data[4] = info->type;
    
    uint8_t * zone_name = info->name;
    for (int8_t i = 0; i < 26; i++)
    {
        out_packet->data[5 + i] = zone_name[i];
        if (zone_name[i] == '\0')
        {
            return;
        }    
    }

    return;
}

void RGBManager::parseGetEffectInfo(hid_read_data_t * in_packet, hid_write_data_t * out_packet)
{
    uint8_t effect_idx = in_packet->read();
    effect_info_t * info = &effect_constructor_list[effect_idx]->info;

    out_packet->size = OPENRGB_PROTOCOL_STANDARD_SIZE;
    out_packet->command = COMMAND_EFFECT_REPORT;
    out_packet->data[0] = effect_idx;
    out_packet->data[1] = info->feature_bitfield;
    out_packet->data[2] = info->supported_modes;
    out_packet->data[3] = info->min_speed;
    out_packet->data[4] = info->max_speed;
    out_packet->data[5] = info->min_brightness;
    out_packet->data[6] = info->max_brightness;
    out_packet->data[7] = info->color_array_min;
    out_packet->data[8] = info->color_array_max;

    for (int i = 0; i < 53; i++)
    {
        out_packet->data[9 + i] = info->name[i];
        if (info->name[i] == '\0') return;
    }

    return;
}

void RGBManager::parseGetLED(hid_read_data_t * in_packet, hid_write_data_t * out_packet)
{
    uint8_t zone_idx = in_packet->read();
    uint8_t led_idx  = in_packet->read();
    uint8_t leds_to_send = ((zone_list[zone_idx]->getInfo()->size - led_idx) > 20) ? 20 : (zone_list[zone_idx]->getInfo()->size - led_idx);
    
    out_packet->size = OPENRGB_PROTOCOL_STANDARD_SIZE;
    out_packet->command = COMMAND_GET_LED;
    out_packet->data[0] = zone_idx;
    out_packet->data[1] = led_idx;
    
    for (uint8_t i = 0; i < leds_to_send; i++)
    {
        out_packet->data[i * 3 + 2] = zone_list[zone_idx]->getLEDR(led_idx + i);
        out_packet->data[i * 3 + 3] = zone_list[zone_idx]->getLEDG(led_idx + i);
        out_packet->data[i * 3 + 4] = zone_list[zone_idx]->getLEDB(led_idx + i);
    }

    return;
}

void RGBManager::parseGetEffect(hid_read_data_t * in_packet, hid_write_data_t * out_packet)
{
    uint8_t zone_idx = in_packet->read();

    out_packet->size = OPENRGB_PROTOCOL_STANDARD_SIZE;
    out_packet->command = COMMAND_GET_EFFECT;
    out_packet->data[0] = zone_idx;
    if (zone_list[zone_idx]->getEffect())
    {
        effect_param_t * sent_effect_param = zone_list[zone_idx]->getEffect()->getParameters();
        out_packet->data[1] = sent_effect_param->index;
        out_packet->data[2] = sent_effect_param->color_mode;
        out_packet->data[3] = sent_effect_param->speed;
        out_packet->data[4] = sent_effect_param->direction_bitfield;
        out_packet->data[5] = sent_effect_param->brightness;
        out_packet->data[6] = sent_effect_param->color_array_size;
        for (uint8_t i = 0; i < sent_effect_param->color_array_size; i++)
        {
            out_packet->data[i * 3 + 7]  = sent_effect_param->color_array[i].r;
            out_packet->data[i * 3 + 8]  = sent_effect_param->color_array[i].g;
            out_packet->data[i * 3 + 9] = sent_effect_param->color_array[i].b;
        }
    }
    else
    {
        out_packet->data[1] = -1;
    }
    
    return;
}

void RGBManager::parseSetEffect(hid_read_data_t * packet)
{
    uint8_t zone_idx = packet->read();
    uint8_t effect_idx =  *(packet->data_pointer);
    zone_list[zone_idx]->setEffect( 
        (*(effect_constructor_list[effect_idx]->constructor))(
          (effect_param_t*)packet->data_pointer, 
           zone_list[zone_idx]
        ));
    return;
}

void RGBManager::parseResize(hid_read_data_t * packet)
{
    uint8_t index = packet->read();
    uint8_t size  = packet->read();
    zone_list[index]->resize(size);
}

void RGBManager::updateZones()
{
    for (int8_t i = 0; i < zone_num; i++)
    {
        zone_list[i]->update();
    }
}

uint8_t RGBManager::getZoneNum()
{
    return zone_num;
}

uint8_t RGBManager::getEffectNum()
{
    return effect_num;
}

void RGBManager::updateEffects()
{
    for (int8_t i = 0; i < zone_num; i++)
    {
        zone_list[i]->updateEffect();
    }
}