# The OpenRGB protocol

## 1 Introduction and terminology

### 1.1 Overview

The objective of the OpenRGB protocol is to provide a flexible and customizable protocol for RGB controllers. It provides a way to describe the device's layout and features, effectively allowing to create custom devices easily and without changes to the host software. 

For now, the OpenRGB protocol can only describe light elements, but in the future an interface for fan devices will also provided. 

The hub is controlled through a HID interface. Packets are used to send data from and to the host. 

### 1.2 Zones

All light elements are grouped in zones or channels. A zone has some parameters, such as its maximum and minimum size, its name, and a list of features it supports. A zone may contain multiple individual LEDs or lights. 

Zones must support accessing and modifying each individual LED. This will henceforth be referred to as "direct mode". 

### 1.3 Effects

Effects are applied on zones and run on the controller, independently of the host computer. The hub may expose a number of effects, each with some information about what parameters they support. It is not required for a device to support any modes other than the aforementioned direct mode.

### 1.4 Describing hardware configuration 

Multiple hardware configurations are supported. For the host software to properly communicate with the hub, several packets are available that describe the layout of the controller:

- A device overview packet describes the general layout of the device: how many zones there are, what firmware revision it is running, what option features it supports, etc. Based on this information, the host requests more information about each individual zone, effect, etc. 
- Zone description packets describe the zone type, how they are called, and what features they support.
- Effect description packets describe what features each effect supports. 

## 2 The interface 

### 2.1 The basic structure

A host can interface with the controller through a HID interface. Packets are used to transmit data packets back and forth. Packets are fixed at 64 bytes in size, the maximum size for full-speed USB devices. 

All packets are structured as follows:

| Index       | Name        | Description                          |
| ----------- | ----------- | ------------------------------------ |
| 0x00        | Packet size | Size of the packet, always 64 / 0x40 |
| 0x01        | Command     | Packet command                       |
| 0x02 - 0x3F | Data        | Packet data                          |

The command byte specifies how the packet must be processed. Packets are sent from the host to the device. Depending on the command, the device may send a response packet back to the host. 

### 2.2 Available commands

| Code | Type (Device out or device in)                | Name                       | Description                                                 |
| ---- | --------------------------------------------- | -------------------------- | ----------------------------------------------------------- |
| 0xD0 | Device out / device in (request + response)   | Device presentation packet | Requests / provides general information about the device.   |
| 0xD1 | Device out / device in (request + response)   | Zone presentation          | Requests / provides information about a zone.               |
| 0xD2 | Device out / device in (request + response)   | Effect presentation        | Requests / provides information about an effect.            |
| 0xE0 | Device in                                     | Apply effect               | Applies an effect to a zone                                 |
| 0xE1 | Device in                                     | Direct mode                | Modifies the LED data of a zone.                            |
| 0xE2 | Device in / Device out                        | Force RGB update           | Forces an update of the RGB zones.                          |
| 0xE3 | Device in                                     | Resize zone                | Resizes an RGB zone (for zones that support resizing).      |
| 0xE4 |                                               |                            |                                                             |
| 0xE5 |                                               |                            |                                                             |
| 0xE6 | Device out / device out (request + response). | Fan presentation           | Requests / provides general information about fan elements. |
| 0xE7 |                                               |                            |                                                             |
| 0xE8 |                                               |                            |                                                             |
| 0xE9 |                                               |                            |                                                             |
| 0xEA |                                               |                            |                                                             |
| 0xEB |                                               |                            |                                                             |
| 0xEC |                                               |                            |                                                             |

### 2.3 A note about interfaces

It is recommended to expose an interface with an OUT endpoint (that is, out from the host computer, in from the device) in addition to the required IN endpoint. This ensures proper timing of the packets, as it forces the host to use INTERRUPT transfers instead of CONTROL transfers. 

## 3 Commands

### Device presentation packet (0xD0)

This packet provides a general overview of the device's capabilities.

#### Request packet

| Index | Value | Description |
| ----- | ----- | ----------- |
| 0     | 0x40  | Packet size |
| 1     | 0xD0  | Command ID  |

#### Response packet

| Index  | Value                         | Description                                |
| ------ | ----------------------------- | ------------------------------------------ |
| 0      | 0x40                          | Packet size                                |
| 1      | 0xD0                          | Command ID                                 |
| 2      | Number of zones               | Number of zones available                  |
| 3      | Number of effects             | Number of effects available                |
| 4      | Reserved                      | In the future, number of fans              |
| 5      | Feature bitfield (0b00000000) | -                                          |
| 6      | Firmware revision             | Firmware revision                          |
| 7 - 63 | Name                          | Device name (null-terminated ASCII string) |

### Zone presentation packet (0xD1)

#### Request packet

| Index | Value      | Description |
| ----- | ---------- | ----------- |
| 0     | 0x40       | Packet size |
| 1     | 0xD1       | Command ID  |
| 2     | Zone index | Zone index  |

#### Response packet

| Index  | Value                         | Description                              |
| ------ | ----------------------------- | ---------------------------------------- |
| 0      | 0x40                          | Packet size                              |
| 1      | 0xD1                          | Command ID                               |
| 2      | Zone index                    | Zone index                               |
| 3      | Feature bitfield (0xA0000000) | A: supports resizing                     |
| 4      | Number of LEDs                | Number of leds in the zone               |
| 5      | Maximum amount of LEDs        | Maximum size of a resizable zone         |
| 6      | Zone type                     | 0: single, 1: linear                     |
| 7 - 64 | Zone name                     | Zone name (null-terminated ASCII string) |

### Effect presentation packet (0xD2)

#### Request packet

| Index | Value        | Description  |
| ----- | ------------ | ------------ |
| 0     | 0x40         | Packet size  |
| 1     | 0xD2         | Command ID   |
| 2     | Effect index | Effect index |

#### Response packet

| Index | Value                         | Description                                                  |
| ----- | ----------------------------- | ------------------------------------------------------------ |
| 0     | 0x40                          | Packet size                                                  |
| 1     | 0xD2                          | Command ID                                                   |
| 2     | Effect index                  | Effect index                                                 |
| 3     | Feature bitfield (0bABCDE000) | A: Mode has speed parameter, B: mode has brightness parameter, C: mode has left/right parameter, D: mode has up/down parameter, E: mode has vertical/horizontal parameter. Note that only one type of direction parameter may be selected. |
| 4     | Mode bitfield (0bABC00000)    | A: no colors, B: random colors, C: mode-specific colors. At least one of the modes must be available. |
| 5     | Min speed                     | Minimum speed value                                          |
| 6     | Max speed                     | Maximum speed value                                          |
| 7     | Min brightness                | Minimum brightness value                                     |
| 8     | Max brightness                | Maximum size of a resizable zone                             |
| 9     | Color array min               | Minimum size of the color array (for mode-specific colors)   |
| 10    | Color array max               | Maximum size of the color array (for mode-specific colors)   |
| 11-63 | Name                          | Effect name (null-terminated ASCII string)                   |

### Apply effect (0xE0)

Applying any effect to a zone is done using the following packet. Any fields not supported by the effect can be ignored. 

| Index  | Value                           | Description                                                |
| ------ | ------------------------------- | ---------------------------------------------------------- |
| 0      | 0x40                            | Packet size                                                |
| 1      | 0xE0                            | Command ID                                                 |
| 2      | Zone index                      | Zone onto which the effect is applied.                     |
| 3      | Effect index                    | Effect to apply                                            |
| 4      | Color mode                      | Either 0x00 (none), 0x01 (random) or 0x02 (mode-specific). |
| 5      | Speed                           | Effect speed                                               |
| 6      | Direction bitfield (0bXYZ00000) | X: left/right, Y: up/down, Z: horizontal/vertical          |
| 7      | Brightness                      | Effect brightness                                          |
| 8      | Mode-specific color array size  | Mode-specific array size                                   |
| 9 - 63 | Mode-specific color array       | Mode-specific color array (encoded in RGB format)          |

Note that the mode-specific color options are ignored when random color mode is selected

### Direct mode (0xE1)

This packet automatically invalidates any mode applied to a zone:

| Index  | Value      | Description                                                  |
| ------ | ---------- | ------------------------------------------------------------ |
| 0      | 0x40       | Packet size                                                  |
| 1      | 0xE1       | Command ID                                                   |
| 2      | Zone index | Zone index                                                   |
| 3      | LED index  | LED index of the first color. Each successive LED index adds 1 to the index |
| 4 - 63 | LED Data   | LED color data in RGB format                                 |

### Force update (0xE2)

Forces an update of the RGB zones. After a certain time of no communication, the device will fall back to automatic update. A response packet is sent for the host computer to sync with the device. 

| Index | Value | Description |
| ----- | ----- | ----------- |
| 0     | 0x40  | Packet size |
| 1     | 0XE2  | Command ID  |

### Resize zone (0xE3)

Resizes a zone.  

| Index | Value      | Description          |
| ----- | ---------- | -------------------- |
| 0     | 0x40       | Packet size          |
| 1     | 0xE3       | Command ID           |
| 2     | Zone index | Zone index           |
| 3     | Size       | New size of the zone |

### Get LED data (0xE4)

Requests LED data from the device. The device will answer with as much LED data as possible.

#### Request packet

| Index | Value      | Description                   |
| ----- | ---------- | ----------------------------- |
| 0     | 0x40       | Packet size                   |
| 1     | 0xE4       | Command ID                    |
| 2     | Zone index | Zone index                    |
| 3     | LED index  | LED index of the first color. |

#### Response packet

| Index  | Value      | Description                                                  |
| ------ | ---------- | ------------------------------------------------------------ |
| 0      | 0x40       | Packet size                                                  |
| 1      | 0xE4       | Command ID                                                   |
| 2      | Zone index | Zone index                                                   |
| 3      | LED index  | LED index of the first color. Each successive LED index adds 1 to the index |
| 4 - 63 | LED Data   | LED color data in RGB format                                 |

### Get effect (0xE5)

Requests current effect from the device. The device will respond with the effect parameters. 

#### Request packet

| Index | Value      | Description |
| ----- | ---------- | ----------- |
| 0     | 0x40       | Packet size |
| 1     | 0xE5       | Command ID  |
| 2     | Zone index | Zone index  |

#### Response packet

| Index  | Value                          | Description                                                  |
| ------ | ------------------------------ | ------------------------------------------------------------ |
| 0      | 0x40                           | Packet size                                                  |
| 1      | 0xE5                           | Command ID                                                   |
| 2      | Zone index                     | Zone onto which the effect is applied                        |
| 3      | Effect index                   | Effect index                                                 |
| 4      | Color mode                     | Color mode: either  0x00 (none), 0x01 (specific), 0x02 (random) |
| 5      | Speed                          | Effect speed                                                 |
| 6      | Direction                      | 0x00: left, 0x01: right, 0x02: up, 3: down, 0x04: horizontal, 0x05: vertical |
| 7      | Brightness                     | Effect brightness                                            |
| 8      | Mode-specific color array size | Mode-specific array size                                     |
| 9 - 63 | Mode-specific color array      | Mode-specific color array (encoded in RGB format)            |

### Fan presentation packet (0xD3)

Requests current effect from the device. The device will respond with the effect parameters. 

#### Request packet

| Index | Value     | Description |
| ----- | --------- | ----------- |
| 0     | 0x40      | Packet size |
| 1     | 0xE6      | Command ID  |
| 2     | Fan index | Fan index   |

#### Response packet

| Index   | Value                                  | Description                         |
| ------- | -------------------------------------- | ----------------------------------- |
| 0       | 0x40                                   | Packet size                         |
| 1       | 0xE6                                   | Command ID                          |
| 2       | Fan index                              | Fan index                           |
| 3       | Features (0xAB000000)                  | A: Has a PWM mode; B: Has a DC mode |
| 4       | Min RPM (PWM mode) (Low byte, 0x00AA)  | Min RPM in PWM mode (low byte)      |
| 5       | Min RPM (PWM mode) (High byte, 0xAA00) | Min RPM in PWM mode (high byte)     |
| 6       | Max RPM (PWM mode) (Low byte, 0x00AA)  | Max RPM in PWM mode (low byte)      |
| 7       | Max RPM (PWM mode) (High byte, 0xAA00) | Max RPM in PWM mode (high byte)     |
| 8       | Min RPM (DC mode) (Low byte, 0x00AA)   | Min RPM in DC mode (low byte)       |
| 9       | Min RPM (DC mode) (High byte, 0xAA00)  | Min RPM in DC mode (high byte)      |
| 10      | Max RPM (DC mode) (Low byte, 0x00AA)   | Max RPM in DC mode (low byte)       |
| 11      | Max RPM (DC mode) (High byte, 0xAA00)  | Max RPM in DC mode (high byte)      |
| 12 - 63 | Name                                   | Fan name                            |

