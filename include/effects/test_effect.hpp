#include "effect.hpp"

class TestEffect : public Effect
{
public:
    TestEffect(effect_param_t * effect_param, Zone * zone_pointer);
    void update();
    void zoneHasBeenResized();
    
private:
    uint32_t period;
    uint32_t last_cycle;
    bool reversed;
};

extern effect_constructor_t TestEffectConstructor;