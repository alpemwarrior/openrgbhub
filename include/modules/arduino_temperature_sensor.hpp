/*

arduino_temperature_sensor.hpp

Temperature sensor implementation (PTC/NTC) for arduino

Author: Pol Rius (alpemwarrior)

*/

#include "sensor.hpp"

/* 
The usual configuration to read an NTC temperature sensor with a microcontroller is 
the following, using a fixed resistor together with the sensor as a voltage divider
to generate a variable voltage.

         Vref
          ^
          |        
          -
         | | Fixed resistor
         | | ?Ω 
          -
          |
          |----> To microcontroller ADC
          |
          -
         | | Sensor
         | | NTC
          -
          |
          v
         GND 

The sensor driver needs to know a few paramaters about this setup to make
sense of the incoming voltage, namely:

    - Base resistance of the thermocouple (at 298K)
    - β Parameter
    - Resistance of the fixed resistor
*/

class ArduinoTempSensor : public Sensor {
public:
    ArduinoTempSensor(uint8_t analog_pin, uint32_t fixed_r, uint32_t nominal_r, uint16_t beta, const char * name);
    void begin();
    uint16_t read();

private:
    uint8_t analog_pin;
    uint32_t fixed_r;
    uint32_t nominal_r;
    uint16_t beta;
};