/*

arduino_pwm_fan.hpp

PWM implementation for arduino

Author: Pol Rius (alpemwarrior)

*/

#ifdef USE_ARDUINO_PWM_FAN

#include <Arduino.h>

#include "circular_buffer.hpp"
#include "fan.hpp"

#define TIME_ARRAY_SIZE 10

typedef enum
{
    LEONARDO_PIN_6,
    LEONARDO_PIN_10
}pwm_fan_out_pin_t;

class ArduinoPWMFan : public Fan {
public:
    ArduinoPWMFan(pwm_fan_out_pin_t out, uint8_t in, const char * name);

    void begin();

    void setSpeedRPM(uint16_t speed);
    void setSpeedAbsolute(uint16_t speed);
    uint16_t getRPMFeedback();

private:
    uint8_t in;
    pwm_fan_out_pin_t out;

    RotBuf<uint32_t, TIME_ARRAY_SIZE> time_buf;
    uint16_t cached_rpm = 0;

    void setPin6Leonardo(uint8_t pwm);
    void setPin10Leonardo(uint8_t pwm);

    void setupPin6Leonardo();
    void setupPin10Leonardo();
    void setupTimersLeonardo();
};

void autoregister_isr(uint8_t pin, RotBuf<uint32_t, TIME_ARRAY_SIZE> * buf);

extern RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_1;
extern RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_2;
extern RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_3;
extern RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_4;
extern RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_5;
extern RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_6;
extern RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_7;

void rpm_isr_handler_1();
void rpm_isr_handler_2();
void rpm_isr_handler_3();
void rpm_isr_handler_4();
void rpm_isr_handler_5();
void rpm_isr_handler_6();
void rpm_isr_handler_7();

inline void rpm_isr_handler_com(RotBuf<uint32_t, TIME_ARRAY_SIZE> * buf);

#endif