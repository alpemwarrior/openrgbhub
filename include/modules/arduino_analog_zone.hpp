/*

arduino_analog_zone.hpp

Zone implementation for analog (4-pin) strips using the arduino framework. 

Author: Pol Rius (alpemwarrior)

*/

#ifdef USE_ARDUINO_ANALOG_ZONE

#pragma once

#include "zone.hpp"

class ArduinoAnalogZone : public Zone
{
public:
    ArduinoAnalogZone(uint8_t pin_r, uint8_t pin_g, uint8_t pin_b, unsigned char * name);

    void begin();
    void update();
    
    void setLED(uint8_t index, uint8_t red, uint8_t green, uint8_t blue);
    uint8_t getLEDR(uint8_t index);
    uint8_t getLEDG(uint8_t index);
    uint8_t getLEDB(uint8_t index);

private:
    uint8_t pin_r, pin_g, pin_b;
    uint8_t red, green, blue;
};

#endif