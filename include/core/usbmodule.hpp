/*

usbmodule.hpp

Interface and utils for usb interfacing

Author: Pol Rius (alpemwarrior)

*/

#pragma once

#include "openrgbprotocol.hpp"

typedef struct 
{
    uint8_t size;
    uint8_t command;
    uint8_t* data_pointer;

    uint8_t read()
    {
        uint8_t tmp = *data_pointer;
        data_pointer++;
        return tmp;
    }

    uint8_t peek()
    {
        return *data_pointer;
    }
    
}hid_read_data_t;

typedef struct __attribute__ ((packed))
{
    union 
    {
        uint8_t raw_data[OPENRGB_PROTOCOL_STANDARD_SIZE];
        struct { uint8_t size; uint8_t command; uint8_t data[OPENRGB_PROTOCOL_STANDARD_SIZE - 2]; };
    };
}hid_write_data_t;

class USBModule
{
public:
    virtual void begin(hid_read_data_t * read_packet, hid_write_data_t * write_packet) = 0;
    virtual bool readPacket(hid_read_data_t * packet) = 0;
    virtual void writePacket(hid_write_data_t * packet) = 0;
};