/*

openrgbhub.hpp

Header for openrgbhub class

Author: Pol Rius (alpemwarrior)

*/

#pragma once

#include "rgbmanager.hpp"
#include "fanmanager.hpp"
#include "sensormanager.hpp"

#define RGB_MANAGER_TARGET_FRAMERATE 60
#define RGB_MANAGER_TARGET_PERIOD (1000 / RGB_MANAGER_TARGET_FRAMERATE)
#define FAN_MANAGER_TARGET_FRAMERATE 20
#define FAN_MANAGER_TARGET_PERIOD (1000 / RGB_MANAGER_TARGET_FRAMERATE)
#define AUTOMATIC_UPDATE_DELAY 100

class OpenRGBHub
{
public:
    OpenRGBHub(const char * name, USBModule * usb_module, RGBManager * rgb_manager, FanManager * fan_manager, SensorManager * sensor_manager);

    void begin();
    void loop();

private:
    void sendDeviceReport(hid_write_data_t * packet);

    const char * name;

    uint32_t last_rgb_update;
    uint32_t last_fan_update;
    uint32_t last_usb_update;

    hid_read_data_t read_packet;
    hid_write_data_t write_packet;

    USBModule     * usb_module;
    RGBManager    * rgb_manager;
    FanManager    * fan_manager;
    SensorManager * sensor_manager;
};

