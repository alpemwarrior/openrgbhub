/*

fan.hpp

Fan interface

Author: Pol Rius (alpemwarrior)

*/

#pragma once

#include "openrgbprotocol.hpp"

class Fan {
public:
    virtual void begin() = 0;
    
    fan_info_t* getInfo();

    void             setState(fan_state_t state);
    fan_state_t*     getState();
    virtual uint16_t getRPMFeedback() = 0;

    void update(uint16_t sensor_val);

protected:
    virtual void setSpeedAbsolute(uint16_t speed) = 0;
    virtual void setSpeedRPM(uint16_t speed) = 0;

    fan_info_t info;

private:
    fan_state_t state;

    uint16_t curveFindVal(uint16_t sensor_val);
};