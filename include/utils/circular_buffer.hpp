/*

circular_buffer.hpp

Stupidly simple circular buffer

Author: Pol Rius (alpemwarrior)

*/

#include <stdint.h>

/* Very primitive FIFO circular buffer */

template <typename T, uint8_t S>
class RotBuf 
{
public:
    T get(uint8_t offset) 
    { 
        return buffer[(index + offset) % S];
    };

    T getHead()
    {
        return get(0);
    }

    T getTail()
    {
        return get(S - 1);
    }
    
    uint8_t size()
    {
        return S;
    }

    void set(uint8_t offset, T value)
    {
        buffer[(index + offset) % S] = value;
    };

    void advance(uint8_t pos)
    {
        index = uint16_t(index + pos) % S;
    }

    void push(T value) 
    {
        set(0, value);
        advance(1);
    }


private:
    uint8_t index = 0;
    T buffer[S];
};